名规则遵循一个统一的规则能够方便团队协作和后期维护

来自渴切网工作中使用的命名规范。
id和class命名采用该版块的英文单词或组合命名，并第一个单词小写，第二个单词首个字母大写，如:keqieCss（最新产品/keqie+Css)
CSS样式表各区块用注释说明
尽量使用英文命名原则
尽量不加中杠和下划线
尽量不缩写，除非一看就明白的单词
css命名适合新手

好的css命名是检验一个css工作者是否合格的一项基本指标。
头：header

内容：content/container

尾：footer

导航：nav

侧栏：sidebar

栏目：col

页面外围控制整体布局宽度：wrapper

左右中：left right center

登录条：loginBar

标志：logo

广告：banner

页面主体：main

热点：hot

新闻：news

下载：download

子导航：subNav

菜单：menu

子菜单：subMenu

搜索：search

友情链接：friendLink

页脚：footer

版权：copyright

滚动：scroll

内容：content

标签页：tab

文章列表：list

提示信息：msg

小技巧：tips

栏目标题：title

加入：joinus

指南：guide

服务：service

注册：register

状态：status

投票：vote

合作伙伴：partner

(二)注释的写法: 
/* Footer */

内容区

/* End Footer */

(三)id的命名: 
(1)页面结构 
容器: container

页头：header

内容：content/container

页面主体：main

页尾：footer

导航：nav

侧栏：sidebar

栏目：col

页面外围控制整体布局宽度：wrapper

左右中：left right center

(2)导航 
导航：nav

主导航：mainNav

子导航：subNav

顶导航：topNav

边导航：sidebar

左导航：leftSidebar

右导航：rightSidebar

菜单：menu

子菜单：subMenu

标题: title

摘要: summary

(3)功能 
标志：logo

广告：banner

登陆：login

登录条：loginBar

注册：register

搜索：search

功能区：shop

标题：title

加入：joinus

状态：status

按钮：btn

滚动：scroll

标签页：tab

文章列表：list

提示信息：msg

当前的: current

小技巧：tips

图标: icon

注释：note

指南：guide

服务：service

热点：hot

新闻：news

下载：download

投票：vote

合作伙伴：partner

友情链接：link

版权：copyright

(四)class的命名: 
(1)标题栏样式,使用"类别+功能"的方式命名,如 
.barNews { }

.barProduct { }

(2)模块结构css定议 
模块标题 .module

模块标题 .moduleHead

模块包装 .moduleWrap

模块内容 .moduleContent

注意事项:

1.一律小写;

2.尽量用英文;

3.不加中杠和下划线;

4.尽量不缩写，除非一看就明白的单词.

主要的 master.css

模块 module.css

基本共用 base.css

布局，版面 layout.css

主题 &nbs