// Javascript 一些常用工具函数
// 2012.12 - 菜根网院长 - http://www.icaigen.com
// 请保留此注释。

var Jtools = {
	
	//判断浏览器类型和版本. eg: alert(Jtools.browser())
	browser : function(){
		var browserName = navigator.userAgent.toLowerCase();
		var myBrowser = {
			version: (browserName.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [0, '0'])[1],
			safari: /webkit/i.test(browserName) && !this.chrome,
			opera: /opera/i.test(browserName),
			firefox: /firefox/i.test(browserName),
			msie: /msie/i.test(browserName) && !/opera/.test(browserName),
			mozilla: /mozilla/i.test(browserName) && !/(compatible|webkit)/.test(browserName) && !this.chrome,
			chrome: /chrome/i.test(browserName) && /webkit/i.test(browserName) && /mozilla/i.test(browserName)
		};
		if(myBrowser.msie){
			return "MSIE - " + myBrowser.version;	
		}else if(myBrowser.mozilla){
        return "FireFox - " + myBrowser.version;
		}else if(myBrowser.opera){
        return "Opera - " + myBrowser.version;
		}else if(myBrowser.chrome){
        return "Chrome - " + myBrowser.version;
		}else if(myBrowser.safari){
        return "Safari - " + myBrowser.version;
		}else{
        return "Unpopular Browser";
		}
	},
	
	//判断客户端操作系统. eg: alert(Jtools.os())
	os : function(){  
		var sUserAgent = navigator.userAgent;    
		var isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel");
		var isUnix = (navigator.platform == "X11") && !isWin && !isMac;
		var isLinux = (String(navigator.platform).indexOf("Linux") > -1);
		var isWin = (navigator.platform == "Win32") || (navigator.platform == "Windows");
		var isAndroid = sUserAgent.indexOf("Android") > -1;
		var isiPhone = sUserAgent.indexOf("iPhone") > -1;
		var isiPad = sUserAgent.indexOf("iPad") > -1;
		var isNokia = sUserAgent.indexOf("NokiaN") > -1;
		if(isAndroid) return "Android";
		if(isiPhone) return "iPhone";
		if(isiPad) return "iPad";
		if(isNokia) return "Symbian";
		if(isMac) return "Mac";
		if(isUnix) return "Unix";  
		if(isLinux) return "Linux";
		if (isWin){  
			var isWin2K = sUserAgent.indexOf("Windows NT 5.0") > -1 || sUserAgent.indexOf("Windows 2000") > -1;
			var isWin2003 = sUserAgent.indexOf("Windows NT 5.2") > -1 || sUserAgent.indexOf("Windows 2003") > -1;
			var isWinVista= sUserAgent.indexOf("Windows NT 6.0") > -1 || sUserAgent.indexOf("Windows Vista") > -1; 
			var isWinXP = sUserAgent.indexOf("Windows NT 5.1") > -1 || sUserAgent.indexOf("Windows XP") > -1;
			var isWin7 = sUserAgent.indexOf("Windows NT 6.1") > -1 || sUserAgent.indexOf("Windows 7") > -1;
			var isWin8 = sUserAgent.indexOf("Windows NT 6.2") > -1 || sUserAgent.indexOf("Windows 8") > -1;
			if(isWin2K) return "Windows 2000";
			if(isWin2003) return "Windows 2003";  
			if(isWinVista) return "Windows Vista";  
			if(isWinXP) return "Windows XP"; 
			if(isWin7) return "Windows 7";
			if(isWin8) return "Windows 8";
		}  
		return "Unpopular Operating System";  
	},

	//判断客户端设备屏幕分辨率. eg: alert(Jtools.resolution().height)
	resolution : function(){
		var hw = [];
		hw['height'] = window.screen.height;
		hw['width'] = window.screen.width;
		return hw;
	},
	
	//获取浏览器窗口宽高. eg: alert(Jtools.windowSize().height)
	windowSize : function(){
		var hw = [];
		hw['width'] = self.innerWidth || (document.documentElement && document.documentElement.clientWidth) || document.body.clientWidth; 
		hw['height'] = self.innerHeight || (document.documentElement && document.documentElement.clientHeight) || document.body.clientHeight;
		return hw;
	},
	
	//获取网页对象（body）实际的宽高. eg: alert(Jtools.scrollSize().height)
	scrollSize : function(){
		var hw = [];
		if(this.offsetSize().height < this.windowSize().height){ //内容区没有占满一屏
			hw['height'] = this.windowSize().height;
		}else{
			hw['height'] = document.body.scrollHeight;
		}
		if(this.offsetSize().width < this.windowSize().width){ //内容区没有占满一屏
			hw['width'] = this.windowSize().width;
		}else{
			hw['width'] = document.body.scrollWidth;
		}
		return hw;
	},
	
	//获取网页内容可见区域的宽高(包括边线). eg: alert(Jtools.offsetSize().height)
	offsetSize : function(){
		var hw = [];
		hw['width'] = document.body.offsetWidth;
		hw['height'] = document.body.offsetHeight;
		return hw;
	},
	
	//获取滚动条位置. eg: alert(Jtools.barPosition().y)
	barPosition : function(){
		var xy = [];
		xy['x'] = self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || document.body.scrollLeft;  
		xy['y'] = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
		return xy;
	},
	
	//获取dom元素的位置. eg: alert(Jtools.domPosition(ele).x)
	domPosition : function(element){
		var xy = [];
		xy['x'] = element.offsetParent ? element.offsetLeft + this.domPosition(element.offsetParent).x : element.offsetLeft;
		xy['y'] = element.offsetParent ? element.offsetTop + this.domPosition(element.offsetParent).y : element.offsetTop;
		return xy;
	},
	
	//获取dom元素相对于父元素的位置. eg: alert(Jtools.domParentPosition(ele).x)
	domParentPosition : function(element){
		var xy = [];
		xy['x'] = element.parentNode == element.offsetParent ? element.offsetLeft : this.domPosition(element).x - this.domPosition(element.parentNode).x; 
		xy['y'] = element.parentNode == element.offsetParent ? element.offsetTop : this.domPosition(element).y - this.domPosition(element.parentNode).y;
		return xy;
	}
};