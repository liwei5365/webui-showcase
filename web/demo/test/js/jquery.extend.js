/**
 * http://www.cnblogs.com/Wayou/p/jquery_plugin_tutorial.html
 */
$.extend({
	sayHello : function(name) {
		console.log('Hello,' + (name ? name : 'Dude') + '!');
	},
	log : function(message) {
		var now = new Date(), y = now.getFullYear(), m = now.getMonth() + 1, // !JavaScript中月分是从0开始的
		d = now.getDate(), h = now.getHours(), min = now.getMinutes(), s = now
				.getSeconds(), time = y + '/' + m + '/' + d + ' ' + h + ':'
				+ min + ':' + s;
		console.log(time + ' My App: ' + message);
	}
});
//调用
$.sayHello();
$.sayHello("小王");
$.log("initializing...");

/**
 * 插件
 * For Example：$("a").myPlugin({"color":"#2C9929"});
 */
;(function($) {
	//TODO 插件1
	$.fn.myPlugin = function(options) {
		 var defaults = {
			        'color': 'red',
			        'fontSize': '12px'
			    };
		 var settings = $.extend({},defaults, options);//将一个空对象做为第一个参数
	    //在这里面,this指的是用jQuery选中的元素
	    //example :$('a'),则this=$('a')
	    this.css({'color': settings.color,"fontSize":settings.fontSize});
	    return this.each(function() {
	        //对每个元素进行操作
	        $(this).append(' ' + $(this).attr('href'));
	    });
	};
})(jQuery);
/**
 * 面向对象的插件开发
 * For Example：$("a").myPlugin({"color":"#2C9929"});
 */
;(function($, window, document,undefined){
	//定义Beautifier的构造函数
	var Beautifier = function(ele, opt) {
	    this.$element = ele,
	    this.defaults = {
	        'color': 'red',
	        'fontSize': '12px',
	        'textDecoration':'none'
	    },
	    this.options = $.extend({}, this.defaults, opt);
	};
	//定义Beautifier的方法
	Beautifier.prototype = {
	    beautify: function() {
	        return this.$element.css({
	            'color': this.options.color,
	            'fontSize': this.options.fontSize,
	            'textDecoration': this.options.textDecoration
	        });
	    }
	};
	//在插件中使用Beautifier对象
	$.fn.myPlugin1 = function(options) {
	    //创建Beautifier的实体
	    var beautifier = new Beautifier(this, options);
	    //调用其方法
	    return beautifier.beautify();
	};
})(jQuery, window, document);